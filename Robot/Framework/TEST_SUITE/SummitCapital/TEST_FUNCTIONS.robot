*** Settings ***
Resource          ../../SETTINGS/AllResource.robot

*** Keywords ***
SummitCapital_001_CalculateCreditLimit
    Set Library Search Order    SeleniumLibrary
    Set Suite Variable    ${FolderData}    SummitCapital
    [ER] Get Path Test Data on Configuration    ${FileConfigPath}${FolderData}${/}${FolderData}_Configuration.xlsx    ${ConfigSheetName}    ROBOT_S_${FolderData}_CalculateCreditLimit
    [EW] Write Cell WebSummitCapital Clear Rows Data To Excel    ${OuputSheetName}
    @{AllListData}    [ER] Get Data All Row For Excel    ${TEST_DATA}    ${InputSheetName}
    ${countAllListData}    Get Length    ${AllListData}
    Set Suite Variable    ${AddRow}    1
    Log to Console    CountAllListData --> ${countAllListData}
    Set Suite Variable    ${CAPTURE_ROW}    2
    FOR    ${i}    IN RANGE    1    ${countAllListData}
        Run Keyword If    '${VERIFY_FLAG}' == 'Y' and '${EXIST_FLAG}' == 'Y'    Run Keyword If Test Failed
        Run Keyword If    '${AllListData}[${i}][0]' != 'Run'    Continue For Loop
        Set Suite Variable    ${RUNNING_STEP}    1
        START TRANSACTION TIME
        [W] Open Browser    เปิดเว็บ    https://www.summitcapital.co.th/index
        [W] Click Element    คลิกเมนู ผลิตภัณฑ์และบริการ    //*[@id="mainmenu2"]
        [W] Click Element    คลิกเมนู คำนวณสินเชื่อ    //ul[@class="dropdown-menuindex-content"]//*[@href="product-calculate"]
        [W] Input Text    กรอก ราคารถ    //input[@id="calculate_price" and @placeholder="กรุณาระบุจำนวนเงิน"]    ${AllListData}[${i}][1]
        [W] Input Text    กรอก เงินดาวน์    //input[@id="calculate_down" and @placeholder="กรุณาระบุจำนวนเงิน"]    ${AllListData}[${i}][2]
        [W] Select From List By Value    เลือก ระยะเวลาผ่อนชำระ    //*[@id="termSelect"]    ${AllListData}[${i}][3]
        [W] Input Text    กรอก \ อัตราดอกเบี้ยต่อเดือน    //*[@id="calculate_increase"]    ${AllListData}[${i}][4]
        [W] Click Element    กดปุ่ม Submit    (//button[@type='submit'][contains(.,'ตกลง')])[1]
        [W] Capture Page Screenshot    CalculateCreditLimit    CalculateCreditLimit
        ${GetInstallment_amount_per_month}=    Get Value    //input[@class='form-control installment']
        Log To Console    Installment amount per month:${GetInstallment_amount_per_month}
        Sleep    3
        Run Keyword If    "${GetInstallment_amount_per_month}" != ""    Log To Console    STATUS PASS
        ...    ELSE    Log To Console    STATUS FAIL
        KEY_ARROW_DOWN    (//button[@type='submit'][contains(.,'ตกลง')])[1]    5
        [W] Input Text    กรอก ค่างวดต่อเดือน    //input[@id="calculate_price2" and @placeholder="กรุณาระบุจำนวนเงิน"]    ${AllListData}[${i}][5]
        [W] Input Text    กรอก เงินดาวน์    //input[@id="calculate_down2" and @placeholder="กรุณาระบุจำนวนเงิน"]    ${AllListData}[${i}][6]
        [W] Select From List By Value    เลือก \ ระยะเวลาผ่อนชำระ    //*[@id="termSelect2"]    ${AllListData}[${i}][7]
        [W] Input Text    กรอก \ อัตราดอกเบี้ยต่อเดือน    //*[@id="calculate_increase2"]    ${AllListData}[${i}][8]
        [W] Capture Page Screenshot    CalculateCreditLimit    CalculateCreditLimit
        [W] Click Element    กดปุ่ม Submit    //button[contains(@onclick,'calculateInstallment2(); return false')]
        ${GetICalculate_the_limit_Installment_Hire_Purchasenstallment_amount_per_month}=    Get Value    //input[contains(@class,'form-control contractAmt')]
        Log To Console    Calculate_the_limit_Installment_Hire_Purchase:${GetICalculate_the_limit_Installment_Hire_Purchasenstallment_amount_per_month}
        Run Keyword If    "${GetICalculate_the_limit_Installment_Hire_Purchasenstallment_amount_per_month}" != ""    Log To Console    STATUS PASS
        ...    ELSE    Log To Console    STATUS FAIL
        ExcelWriteWebSummitCapitalFunctions.[EW] Write Cell Calculate Credit Limit To Excel    OUTPUT    ${AddRow}    ${AllListData}[${i}][1]    ${AllListData}[${i}][2]    ${AllListData}[${i}][3]    ${AllListData}[${i}][4]    ${GetInstallment_amount_per_month}    ${AllListData}[${i}][5]    ${AllListData}[${i}][6]    ${AllListData}[${i}][7]    ${AllListData}[${i}][8]    ${GetICalculate_the_limit_Installment_Hire_Purchasenstallment_amount_per_month}
        Set Suite Variable    ${CAPTURE_ROW}    ${${CAPTURE_ROW} + 1}
        Set Suite Variable    ${EXIST_FLAG}    Y
    END
    Comment    EXPORT EXCEL RESULT

KEY_ARROW_DOWN
    [Arguments]    ${Element}    ${Times}
    FOR    ${i}    IN RANGE    1    ${Times}
        Press Keys    ${Element}    ARROW_DOWN
        Sleep    1
    END
