*** Settings ***
Suite Setup       GLOBAL SET
Suite Teardown
Test Setup        Log To Console    ********** START **********
Test Teardown     Log To Console    ********** END **********
Resource          TEST_FUNCTIONS.robot
Resource          ../../SETTINGS/AllResource.robot

*** Test Cases ***
SummitCapital_CalculateCreditLimit
    [Tags]    SummitCapital_CalculateCreditLimit
    Run Keyword    SummitCapital_001_CalculateCreditLimit
