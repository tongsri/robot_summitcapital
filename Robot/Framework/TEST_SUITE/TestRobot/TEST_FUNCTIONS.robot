*** Settings ***
Resource          ../../SETTINGS/AllResource.robot

*** Keywords ***
WebSummitCapital_001_CalculateCreditLimit
    Set Library Search Order    SeleniumLibrary
    Set Suite Variable    ${FolderData}    TestRobot
    [ER] Get Path Test Data on Configuration    ${FileConfigPath}${FolderData}${/}${FolderData}_Configuration.xlsx    ${ConfigSheetName}    ROBOT_S_${FolderData}_WebSummitCapital
    [EW] Write Cell WebSummitCapital Clear Rows Data To Excel    ${OuputSheetName}
    @{AllListData}    [ER] Get Data All Row For Excel    ${TEST_DATA}    ${InputSheetName}
    ${countAllListData}    Get Length    ${AllListData}
    Set Suite Variable    ${AddRow}    1
    Log to Console    CountAllListData --> ${countAllListData}
    Set Suite Variable    ${CAPTURE_ROW}    2
    FOR    ${i}    IN RANGE    1    ${countAllListData}
        Run Keyword If    '${VERIFY_FLAG}' == 'Y' and '${EXIST_FLAG}' == 'Y'    Run Keyword If Test Failed
        Run Keyword If    '${AllListData}[${i}][0]' != 'Run'    Continue For Loop
        Set Suite Variable    ${RUNNING_STEP}    1
        START TRANSACTION TIME
        [W] Open Browser    เปิดเว็บ    https://www.summitcapital.co.th/index
        Set Suite Variable    ${CAPTURE_ROW}    ${${CAPTURE_ROW} + 1}
        Set Suite Variable    ${EXIST_FLAG}    Y
    END
    Comment    EXPORT EXCEL RESULT
