*** Settings ***
Suite Setup       GLOBAL SET
Test Setup        Log To Console    ********** START **********
Test Teardown     Log To Console    ********** END **********
Resource          ../../SETTINGS/AllResource.robot
Resource          TEST_FUNCTIONS.robot

*** Test Cases ***
TestRobot_WebSummitCapital
    [Tags]    TestRobot_WebSummitCapital
    Run Keyword     WebSummitCapital_001_CalculateCreditLimit
