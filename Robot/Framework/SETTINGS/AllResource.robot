*** Settings ***
Resource          ../KEYWORDS/EXCEL/ExcelReadFunctions.robot
Resource          ../KEYWORDS/EXCEL/ExcelWriteFunctions.robot
Resource          ../KEYWORDS/GENERAL/GeneralFunctions.robot
Resource          ../KEYWORDS/MOBILE/MobileFunctions.robot
Resource          ../KEYWORDS/WEB/WebFunctions.robot
Resource          ../VARIABLES/GlobalVariables.robot
Resource          StandardLibrary.robot
Resource          ../KEYWORDS/EXCEL/ExcelWriteSummitCapitalFunctions.robot
Resource          ../KEYWORDS/EXCEL/ExcelWriteWebSummitCapitalFunctions.robot
