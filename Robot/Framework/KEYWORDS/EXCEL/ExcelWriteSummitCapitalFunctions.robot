*** Settings ***
Resource          ../../SETTINGS/StandardLibrary.robot
Resource          ../../SETTINGS/AllResource.robot

*** Keywords ***
[EW] Write Cell SummitCapital Clear Rows Data To Excel
    [Arguments]    ${SheetName}
    Set Library Search Order    ExcelRobot
    Log to Console    Open Write Cell SummitCapital Clear Rows Data To Excel # --> ${TEST_DATA}
    ${CountColumn}    ${CountRow}    [ER] Open Excel    ${TEST_DATA}    ${SheetName}
    Log to Console    CountColumn --> ${CountColumn}
    Log to Console    CountRow --> ${CountRow}
    Open Excel To Write    ${TEST_DATA}
    FOR    ${i}    IN RANGE    1    ${${CountRow} + 1}
        [EW] Write Cell WebSummitCapital Clear Columns Data To Excel    ${SheetName}    ${i}    ${CountColumn}
    END
    Save Excel
    Set Suite Variable    ${RUNNING_STEP}    1

[EW] Write Cell SummitCapital Clear Columns Data To Excel
    [Arguments]    ${SheetName}    ${Row}    ${CountColumn}
    Set Library Search Order    ExcelRobot
    Log to Console    Open Excel To Write Cell SummitCapital Clear Columns Data To Excel # --> ${TEST_DATA} ${SheetName} ${Row}
    FOR    ${i}    IN RANGE    0    ${CountColumn}
        Log To Console    Get Data & Clear [${Row}][${i}]
        Write To Cell    ${SheetName}    ${i}    ${Row}    ${EMPTY}
    END

[EW] Write Cell Calculate Credit Limit To Excel
    [Arguments]    ${SheetName}    ${Row}    ${Car_price}    ${down_payment}    ${Installment_period}    ${Interest_rate_per_month}    ${Installment_amount_per_month}    ${Calculate_the_limit_Monthly_installment}    ${Calculate_the_limit_down_payment}    ${Calculate_the_limit_Installment_period}    ${Calculate_the_limit_Interest_rate_per_month}    ${Calculate_the_limit_Installment_Hire_Purchase}
    Set Library Search Order    ExcelRobot
    Log to Console    Open Excel To Write Cell Calculate Credit Limit To Excel # --> ${TEST_DATA}
    Open Excel To Write    ${TEST_DATA}
    Write To Cell By Name    ${SheetName}    A${${Row}+1}    ${TS_STATUS}
    Run Keyword If    "${TS_MESSAGE}" == "None"    Write To Cell By Name    ${SheetName}    B${${Row}+1}    ${EMPTY}
    Run Keyword If    "${TS_MESSAGE}" != "None"    Write To Cell By Name    ${SheetName}    B${${Row}+1}    ${TS_MESSAGE}
    Write To Cell By Name    ${SheetName}    C${${Row}+1}    ${START_TIME}
    END TRANSACTION TIME
    Run Keyword If    "${START_TIME}" != ""    Write To Cell By Name    ${SheetName}    D${${Row}+1}    ${END_TIME}
    Write To Cell By Name    ${SheetName}    E${${Row}+1}    ${CAPTURE_SCREEN_SHOT}
    Write To Cell By Name    ${SheetName}    F${${Row}+1}    ${Car_price}
    Write To Cell By Name    ${SheetName}    G${${Row}+1}    ${down_payment}
    Write To Cell By Name    ${SheetName}    H${${Row}+1}    ${Installment_period}
    Write To Cell By Name    ${SheetName}    I${${Row}+1}    ${Interest_rate_per_month}
    Write To Cell By Name    ${SheetName}    J${${Row}+1}    ${Installment_amount_per_month}
    Write To Cell By Name    ${SheetName}    K${${Row}+1}    ${Calculate_the_limit_Monthly_installment}
    Write To Cell By Name    ${SheetName}    L${${Row}+1}    ${Calculate_the_limit_down_payment}
    Write To Cell By Name    ${SheetName}    M${${Row}+1}    ${Calculate_the_limit_Installment_period}
    Write To Cell By Name    ${SheetName}    N${${Row}+1}    ${Calculate_the_limit_Interest_rate_per_month}
    Write To Cell By Name    ${SheetName}    O${${Row}+1}    ${Calculate_the_limit_Installment_Hire_Purchase}
    Save Excel
    Set Suite Variable    ${AddRow}    ${${Row} + 1}
    Set Suite Variable    ${START_TIME}    ${EMPTY}
    Set Suite Variable    ${END_TIME}    ${EMPTY}
