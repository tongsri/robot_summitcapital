*** Settings ***
Resource          ../../SETTINGS/StandardLibrary.robot
Resource          ../../SETTINGS/AllResource.robot

*** Keywords ***
[EW] Get Data All Row For Excel
    [Arguments]    ${Filename}    ${SheetName}    ${TestCase}
    Set Library Search Order    ExcelRobot
    ${CountColumn}    ${CountRow}    [ER] Open Excel    ${Filename}    ${SheetName}
    ${TEST_EXECUTE}    Create List
    FOR    ${i}    IN RANGE    1    ${CountRow}
        ${Return}    [ER] Read Cell Data Column For Excel    ${SheetName}    ${CountColumn}    ${i}
        Append To List    ${TEST_EXECUTE}    ${Return}
    END
    Log to Console    TEST_EXECUTE --> ${TEST_EXECUTE}
    Set Suite Variable    ${currentRowController}    0
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${EMPTY}
    FOR    ${i}    IN    @{TEST_EXECUTE}
        Set Suite Variable    ${currentRowController}    ${${currentRowController} + 1}
        LOG    "@{i}[0]"!="${TestCase}"
        Run Keyword If    "@{i}[0]"!="${TestCase}"    Continue For Loop
        LOG    "@{i}[2]"=="No Run"
        Run Keyword If    "@{i}[2]"=="No Run"    Continue For Loop
        ${Date}    Get Current Date
        Set Suite Variable    ${testCaseStep}    ${TestCase}
        ${STATUS}    ${ERROR}    Run Keyword And Ignore Error    @{i}[1]    @{i}[0]    @{i}[1]
        Log To Console    Sheet Name --> ${SheetName}
        [EW] Write Cell Status To Excel    ${SheetName}    C    ${currentRowController}    ${STATUS}
        Run Keyword If    "${STATUS}"=="FAIL"    [EW] Write Cell Error To Excel    ${SheetName}    D    ${currentRowController}    ${ERROR_DESCRIPTION}
        Set Suite Variable    ${TOTAL_EXECUTED}    ${${TOTAL_EXECUTED} + 1}
        Log To Console    TOTAL_EXECUTED --> ${TOTAL_EXECUTED}
        Run Keyword If    "${STATUS}"=="FAIL"    Set Suite Variable    ${EXECUTE_STATUS}    FAIL
        Run Keyword If    "${STATUS}"=="FAIL"    Run Keyword And Continue On Failure    FAIL
    END

[EW] Open Excel To Write
    [Arguments]    ${Filename}    ${SheetName}    ${RESULTSheet}
    Set Library Search Order    ExcelRobot
    ${Filename}    Join Path    ${Filename}
    Comment    ${DIR}    Replace String    ${CURDIR}    RF_Framework\\FRAMEWORKS    RF_Framework
    Comment    ${DIR}    Replace String    ${DIR}    RF_Framework/FRAMEWORKS    RF_Framework
    Comment    ${DIR}    Remove String    ${DIR}    //FRAMEWORKS
    Log to Console    [EW] Open Excel To WriteDIR --> ${DIR}
    ${FileExcelWrite}    Set Variable    ${DIR}${/}${Filename}
    ${newResultFolder}    Set Variable    ${Foldername}
    ${newFileExcelWrite}    Set Variable    ${Foldername}${/}${RESULTSheet}.xlsx
    Open Excel    ${FileExcelWrite}
    ${dateVersion}=    Read Cell Data By Name    SummaryReport    B9
    ${execute}=    Read Cell Data By Name    SummaryReport    B10
    ${currentDate}=    Get Current Date    result_format=%Y%m%d
    Set Suite Variable    ${EXECUTE_VERSION}    ${execute}
    Set Suite Variable    ${EXECUTE_STATUS}    PASS
    Log To Console    Date Version --> ${dateVersion}
    Log To Console    Execute Version --> ${EXECUTE_VERSION}
    Log To Console    Current Version --> ${currentDate}
    Run Keyword If    "${dateVersion}" != "${currentDate}"    [EW] Write Cell DateVersion To Excel    ${FileExcelWrite}    SummaryReport    ${currentDate}
    ${executeVersion}=    Convert To Integer    ${EXECUTE_VERSION}
    [EW] Write Cell ExecuteVersion To Excel    ${FileExcelWrite}    SummaryReport    ${${executeVersion} + 1}
    Open Excel To Write    ${FileExcelWrite}    ${newFileExcelWrite}
    Set Suite Variable    ${FileExcelWrite}
    Set Suite Variable    ${newFileExcelWrite}
    Set Suite Variable    ${newResultFolder}
    Create Sheet    ${RESULTSheet}
    Save Excel

[EW] Write Cell Error For Excel
    [Arguments]    ${SheetName}    ${Column}    ${Title}    ${Row}    ${ERROR}
    Set Library Search Order    ExcelRobot
    ${Row}    Evaluate    ${Row}+1
    Write To Cell By Name    ${SheetName}    ${Column}1    ${Title}
    Write To Cell By Name    ${SheetName}    ${Column}${Row}    ${ERROR}
    Save Excel

[EW] Write Cell Status For Excel
    [Arguments]    ${SheetName}    ${Column}    ${Title}    ${Row}    ${STATUS}
    Set Library Search Order    ExcelRobot
    ${Row}    Evaluate    ${Row}+1
    Write To Cell By Name    ${SheetName}    ${Column}1    ${Title}
    Write To Cell By Name    ${SheetName}    ${Column}${Row}    ${STATUS}
    Save Excel

[EW] Write Cell Test Case For Excel
    [Arguments]    ${SheetName}    ${Column}    ${Title}    ${Row}    ${TESTCASENAME}
    Set Library Search Order    ExcelRobot
    ${Row}    Evaluate    ${Row}+1
    Write To Cell By Name    ${SheetName}    ${Column}1    ${Title}
    Write To Cell By Name    ${SheetName}    ${Column}${Row}    ${TESTCASENAME}
    Save Excel

[EW] Write Cell Message For Excel
    [Arguments]    ${SheetName}    ${Column}    ${Title}    ${Row}    ${Msg}
    Set Library Search Order    ExcelRobot
    ${Row}    Evaluate    ${Row}
    Log to Console    SheetName --> ${SheetName}
    Log to Console    Column --> ${Column}
    Log to Console    Title --> ${Title}
    Log to Console    Row --> ${Row}
    Log to Console    Msg --> ${Msg}
    Comment    Write To Cell By Name    ${SheetName}    ${Column}1    ${Title}
    Write To Cell By Name    ${SheetName}    ${Column}${Row}    ${Msg}
    Save Excel

[EW] Write Cell Message To Excel
    [Arguments]    ${Filenamexxxx}    ${SheetName}    ${Column}    ${Title}    ${Row}    ${Msg}
    Set Library Search Order    ExcelRobot
    ${Filename}    Join Path    ${Filename}
    ${DIR}    Remove String    ${CURDIR}    Keywork
    ${DIR}    Set Variable    ${DIR}${/}${Filename}
    Log to Console    Open Excel# --> ${Filename}
    Open Excel To Write    ${Filename}
    Comment    Write To Cell By Name    ${SheetName}    ${Column}1    ${Title}
    Write To Cell By Name    ${SheetName}    ${Column}${${Row}+1}    ${Msg}
    Save Excel

[EW] Write Cell StartStep To Excel
    [Arguments]    ${SheetName}    ${Row}    ${StepName}
    Set Library Search Order    ExcelRobot
    Log to Console    Open Excel To Write StartStep # --> ${Filename}
    Open Excel To Write    ${Filename}
    Write To Cell By Name    ${SheetName}    A${${Row}+1}    ${testcaseStep}
    Write To Cell By Name    ${SheetName}    B${${Row}+1}    ${runningStep}# ${StepName}
    ${Date}    Get Current Date
    Write To Cell By Name    ${SheetName}    E${${Row}+1}    ${Date}
    Save Excel

[EW] Write Cell EndStep To Excel
    [Arguments]    ${SheetName}    ${Row}    ${Status}    ${ScreenShot}
    Set Library Search Order    ExcelRobot
    Log to Console    Open Excel To Write EndStep # --> ${Filename}
    Open Excel To Write    ${Filename}
    Write To Cell By Name    ${SheetName}    C${${Row}+1}    ${Status}
    Run Keyword If    "${ScreenShot}" != "None"    Write To Cell By Name    ${SheetName}    D${${Row}+1}    ${ScreenShot}
    ${Date}    Get Current Date
    Write To Cell By Name    ${SheetName}    F${${Row}+1}    ${Date}
    Write To Cell By Name    ${SheetName}    G${${Row}+1}    ${ERROR_DESCRIPTION}
    Save Excel

[EW] Write Cell StartTransaction To Excel
    [Arguments]    ${SheetName}    ${Row}
    Set Library Search Order    ExcelRobot
    Log to Console    Open Excel To Write StartTestCase # --> ${TEST_DATA}
    Open Excel To Write    ${TEST_DATA}
    ${Date}    Get Current Date    result_format=%d/%m/%Y %H:%M:%S:%f
    Write To Cell By Name    ${SheetName}    C${${Row}+1}    ${Date}
    Save Excel
    Set Suite Variable    ${EndTransactionRow}    ${${Row}}

[EW] Write Cell EndTransactionTo Excel
    [Arguments]    ${SheetName}    ${Row}
    Set Library Search Order    ExcelRobot
    Log to Console    Open Excel To Write EndTestCase # --> ${TEST_DATA}
    Open Excel To Write    ${TEST_DATA}
    ${Date}    Get Current Date    result_format=%d/%m/%Y %H:%M:%S:%f
    Write To Cell By Name    ${SheetName}    D${${Row}+1}    ${Date}
    Save Excel

[EW] Write Cell StartSummary To Excel
    [Arguments]    ${SheetName}
    Set Library Search Order    ExcelRobot
    Log to Console    Open Excel To Write StartSummary # --> ${Filename}
    Open Excel To Write    ${Filename}
    Write To Cell By Name    ${SheetName}    B2    ${EXECUTED_DATE}
    ${StartDate}    Get Current Date    result_format=%H:%M:%S
    Log To Console    Summary Start Date --> ${StartDate}
    ${StartTime}    Convert Time    ${StartDate}
    Log To Console    Summary Start Time --> ${StartTime}
    Set Suite Variable    ${START_TIME}    ${StartTime}
    Write To Cell By Name    ${SheetName}    B3    ${StartDate}
    Save Excel

[EW] Write Cell EndSummary To Excel
    [Arguments]    ${SheetName}
    Set Library Search Order    ExcelRobot
    Log to Console    Open Excel To Write EndSummary # --> ${Filename}
    Open Excel To Write    ${Filename}
    ${EndDate}    Get Current Date    result_format=%H:%M:%S
    Log To Console    Summary End Date --> ${EndDate}
    ${EndTime}    Convert Time    ${EndDate}
    Log To Console    Summary End Time --> ${EndTime}
    Set Suite Variable    ${END_TIME}    ${EndTime}
    Write To Cell By Name    ${SheetName}    B4    ${EndDate}
    ${StartTime}    Convert To Integer    ${START_TIME}
    ${EndTime}    Convert To Integer    ${END_TIME}
    ${DurationTime}    Evaluate    ${EndTime} - ${StartTime}
    Log To Console    Summary Duration Time --> ${DurationTime}
    ${DurationHour}=    Evaluate    round(${DurationTime} / 3600)
    ${DurationMinute}=    Evaluate    round(${DurationTime} / 60)
    ${DurationSecond}=    Evaluate    ${DurationTime}%60
    ${DurationHour}=    [EW] Add Zero    ${DurationHour}
    ${DurationMinute}=    [EW] Add Zero    ${DurationMinute}
    ${DurationSecond}=    [EW] Add Zero    ${DurationSecond}
    ${StrDurationTime}=    Catenate    SEPARATOR=:    ${DurationHour}    ${DurationMinute}    ${DurationSecond}
    ${DurationTime}    Convert To String    ${StrDurationTime}
    Write To Cell By Name    ${SheetName}    B5    ${DurationTime}
    ${totalExecuted}    Convert To String    ${TOTAL_EXECUTED}
    ${totalPassed}    Convert To String    ${TOTAL_PASSED}
    ${totalFailed}    Convert To String    ${TOTAL_FAILED}
    Write To Cell By Name    ${SheetName}    B6    ${totalExecuted}
    Write To Cell By Name    ${SheetName}    B7    ${totalPassed}
    Write To Cell By Name    ${SheetName}    B8    ${totalFailed}
    Save Excel

[EW] Write Cell Status To Excel
    [Arguments]    ${SheetName}    ${Column}    ${Row}    ${STATUS}
    Set Library Search Order    ExcelRobot
    Comment    ${Row}    Evaluate    ${Row}+1
    Log to Console    Open Excel To Write Status # --> ${TEST_DATA}
    Open Excel To Write    ${TEST_DATA}
    Write To Cell By Name    ${SheetName}    ${Column}${${Row} + 1}    ${STATUS}
    Save Excel

[EW] Write Cell Error To Excel
    [Arguments]    ${SheetName}    ${Column}    ${Row}    ${ERROR}
    Set Library Search Order    ExcelRobot
    Comment    ${Row}    Evaluate    ${Row}+1
    Log to Console    Open Excel To Write Error # --> ${TEST_DATA}
    Open Excel To Write    ${TEST_DATA}
    Write To Cell By Name    ${SheetName}    ${Column}${${Row} + 1}    ${ERROR}
    Save Excel

[EW] Write Cell DateVersion To Excel
    [Arguments]    ${Filename}    ${SheetName}    ${Value}
    Set Library Search Order    ExcelRobot
    Log to Console    Open Excel To Write Date Version # --> ${Filename}
    Open Excel To Write    ${Filename}
    ${current}    Convert To String    ${Value}
    Write To Cell By Name    ${SheetName}    B9    ${current}
    ${execute}    Convert To String    0
    Write To Cell By Name    ${SheetName}    B10    ${execute}
    Save Excel
    Set Suite Variable    ${EXECUTE_VERSION}    ${execute}

[EW] Write Cell ExecuteVersion To Excel
    [Arguments]    ${Filename}    ${SheetName}    ${Value}
    Set Library Search Order    ExcelRobot
    Log to Console    Open Excel To Write Execute Version # --> ${Filename}
    Open Excel To Write    ${Filename}
    ${execute}    Convert To String    ${Value}
    Set Suite Variable    ${EXECUTE_VERSION}    ${execute}
    Write To Cell By Name    ${SheetName}    B10    ${execute}
    Save Excel

[EW] Add Zero
    [Arguments]    ${Value}
    Log To Console    Value --> ${Value}
    ${Result}=    Set Variable If    ${Value} < 10    "${0}${Value}"    "${Value}"
    Log To Console    Result --> ${Result}
    ${Return}=    Convert To String    ${Result}
    ${Result}=    Replace String    ${Return}    "    ${EMPTY}
    Log To Console    Add Zero --> ${Result}
    [Return]    ${Result}

[EW] Get Path Test Data on Configuration
    [Arguments]    ${Filename}    ${SheetName}
    Set Library Search Order    ExcelRobot
    Log To Console    Suite Name --> ${Suite Name}
    Log To Console    Test Name --> ${TestName}
    Log To Console    File Name --> ${Filename}
    ${CountColumn}    ${CountRow}    [ER] Open Excel    ${Filename}    ${SheetName}
    @{TEST_EXECUTE}    Create List
    FOR    ${i}    IN RANGE    1    ${CountRow}
        ${Return}    [ER] Read Cell Data Column For Excel    ${SheetName}    ${CountColumn}    ${i}
        Append To List    ${TEST_EXECUTE}    ${Return}
    END
    Log To Console    List Script Name --> ${TEST_EXECUTE}
    ${TestCase}    Replace String    ${TestName}    ${SPACE}    _
    ${TestCase}    Catenate    SEPARATOR=    ROBOT_S_    ${TestCase}
    Log To Console    ${TestCase}
    FOR    ${i}    IN    @{TEST_EXECUTE}
        ${ScriptName}    Set Variable    ${i}[0]
        ${TestData}    Set Variable    ${i}[1]
        Run Keyword If    "${TestCase}" != "${ScriptName}"    Continue For Loop
        Log To Console    Used --> ${TestData}
        Set Suite Variable    ${TEST_DATA}    ${TestData}
    END
